<?php

namespace Drupal\yandex_real_estate_xml_json_converter;

  const FILE_URL = 'https://storage.yandexcloud.net/atrealt-feeds/export/6699a1e80c1c7832f9a8fa99dbb7c8cd/site/all.xml';

/**
 * Class GetXml
 *
 * @package Drupal\yandex_real_estate_xml_images_downloader
 */
class ConvertXmlJson {

  // Массив XML.
  private $xml_array = [];

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    // Получаем массив XML из файла.
    $xml_tmp = \Drupal::service('yandex_real_estate_xml_images_downloader.get_xml')->getXml();
    if (is_array($xml_tmp)) {
      foreach ($xml_tmp['offer'] as $items) {
        foreach ($items as $key => $item) {
          if ($key == '@attributes') {
            $items['internal-id'] = $item['internal-id'];
          }
          if ($key == 'image') {
            $items['image'] = [];
            if (is_array($item)) {
              foreach ($item as $file_url) {
                $tmp = explode("/", $file_url);
                $name = end($tmp);
                $uri = 'public://' . $name;
                if (file_exists($uri)) {
                  $file_tmp = \Drupal::entityTypeManager()
                    ->getStorage('file')
                    ->loadByProperties(['uri' => $uri]);
                  $file = reset($file_tmp) ?: NULL;
                }
                $items['image'][] = $file->uuid->value;
              }
            }
          }
        }
        $this->xml_array[] = $items;
      }
    }
  }

  /**
   * Метод, возвращающий XML
   */
  public function convertXmlJson() {
    return $this->xml_array;
  }

}
