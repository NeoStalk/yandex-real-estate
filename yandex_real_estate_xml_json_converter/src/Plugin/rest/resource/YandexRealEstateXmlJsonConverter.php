<?php

namespace Drupal\yandex_real_estate_xml_json_converter\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "yandex_real_estate_jxml_son_converter",
 *   label = @Translation("Yandex Real Estate XML to JSON Converter"),
 *   uri_paths = {
 *     "canonical" = "/api/rest/get-yandex-real-estate-convert--xml-json"
 *   }
 * )
 */
class YandexRealEstateXmlJsonConverter extends ResourceBase {

  public function permissions() {
    return [];
  }

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {

    $xml_array = \Drupal::service('yandex_real_estate_xml_json_converter.convert_xml_json')->convertXmlJson();
    return new ResourceResponse($xml_array);
  }

}
