<?php

namespace Drupal\yandex_real_estate_xml_images_downloader\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Dummy settings for this site.
 */
class BatchForm extends FormBase {

  /**
   * Batch Builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;
  protected $files_url;


  /**
   * BatchForm constructor.
   */
  public function __construct() {

    $this->batchBuilder = new BatchBuilder();
    $xml = \Drupal::service('yandex_real_estate_xml_images_downloader.get_xml')->getXml();
    if (!empty($xml)) {
      foreach ($xml['offer'] as $offer) {
        if (!empty($offer['image']) && is_array($offer['image'])) {
          foreach ($offer['image'] as $file_url) {
            $this->files_url[] = $file_url;
          }
        }
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yandex_real_estate_xml_images_downloader';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Запустить'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->batchBuilder
      ->setTitle($this->t('Processing'))
      ->setInitMessage($this->t('Initializing.'))
      ->setProgressMessage($this->t('Completed @current of @total.'))
      ->setErrorMessage($this->t('An error has occurred.'));

    $this->batchBuilder->setFile(drupal_get_path('module', 'yandex_real_estate_xml_images_downloader') . '/src/Form/BatchForm.php');
    $this->batchBuilder->addOperation([$this, 'processItems'], [$this->files_url]);
    $this->batchBuilder->setFinishCallback([$this, 'finished']);

    batch_set($this->batchBuilder->toArray());
  }

  /**
   * Processor for batch operations.
   */
  public function processItems($items, array &$context) {
    // Elements per operation.
    $limit = 1;

    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($items);
    }

    // Save items to array which will be changed during processing.
    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $items;
    }

    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      // Remove already processed items.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, $limit);
      }

      foreach ($context['sandbox']['items'] as $item) {
        if ($counter != $limit) {
          $this->processItem($item);
          $counter++;
          $context['sandbox']['progress']++;
          $context['message'] = $this->t('Обработано файлов :progress из :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);

          // Increment total processed item values. Will be used in finished
          // callback.
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }
    }

    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Process single item.
   *
   * @param int|string $nid
   *   An id of Node.
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   An object with new published date.
   */
  public function processItem($file_url) {
    $tmp = explode("/", $file_url);
    $name = end($tmp);
    $uri = 'public://' . $name;
    if (!file_exists($uri)) {
      $image = file_get_contents($file_url);
      if ($image) {
#       $file = \Drupal::service('file.repository')->writeData($image, $uri, 'FILE_EXISTS_REPLACE');//Для 9-ки//
        $file = file_save_data($image, $uri, 'FILE_EXISTS_REPLACE');
      }
    }
  }

  /**
   * Finished callback for batch.
   */
  public function finished($success, $results, $operations) {
    $message = $this->t('Обработано файлов: @count', [
      '@count' => $results['processed'],
    ]);
    $this->messenger()->addStatus($message);
  }

}
