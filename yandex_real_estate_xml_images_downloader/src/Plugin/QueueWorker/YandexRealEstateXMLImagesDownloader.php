<?php

/**
 * @file
 * Contains \Drupal\yandex_real_estate_xml_images_downloader\Plugin\QueueWorker\YandexRealEstateXMLImagesDownloader.
 */

namespace Drupal\yandex_real_estate_xml_images_downloader\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;


/**
 * Process a queue of media items to fetch their thumbnails.
 *
 * @QueueWorker(
 *   id = "queue_image_downloader",
 *   title = @Translation("Yandex Real Estate XML Images Downloader"),
 *   cron = {"time" = 130}
 * )
 */
class YandexRealEstateXMLImagesDownloader extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    $image = file_get_contents($data['file_url']);
    $uri = $data['uri'];
    if ($image) {
    #       $file = \Drupal::service('file.repository')->writeData($image, $uri, 'FILE_EXISTS_REPLACE');//Для 9-ки//
      $file = file_save_data($image, $uri, 'FILE_EXISTS_REPLACE');
    }
  }

}
