<?php

namespace Drupal\yandex_real_estate_xml_images_downloader;

  const FILE_URL = 'https://storage.yandexcloud.net/atrealt-feeds/export/6699a1e80c1c7832f9a8fa99dbb7c8cd/site/all.xml';

/**
 * Class GetXml
 *
 * @package Drupal\yandex_real_estate_xml_images_downloader
 */
class GetXml {
  // Массив XML.
  private $xml_array;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    // Получаем массив XML из файла.
    $xml = simplexml_load_file(FILE_URL);
    $this->xml_array = unserialize(serialize(json_decode(json_encode((array) $xml), 1)));
  }

  /**
   * Метод, который возвращает XML
   */
  public function getXml() {
    return $this->xml_array;
  }

}
